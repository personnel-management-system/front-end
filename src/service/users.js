import axios from 'axios'
const url = 'http://127.0.0.1:3000/user/'
const userService = {
    login(username, password) {
        return axios.post(url + 'login', { username, password })
    },
    logout() {
        localStorage.clear()
    },
    checkTypeLogin() {
        if (localStorage.user !== undefined) {
            const users = JSON.parse(localStorage.user)
            if (users.type === 'student') {
                return 1
            } else if (users.type === 'admin') {
                return 2
            } else {
                return 0
            }
        } else {
            return 0
        }
    },
    getUserDataLocal() {
        if (localStorage.user !== undefined) {
            return JSON.parse(localStorage.user)
        } else {
            return 0
        }
    },
    getUserIdLogin() {
        const users = JSON.parse(localStorage.user)
        return users._id
    },
    getUserDataLogin() {
        const users = JSON.parse(localStorage.user)
        return axios.get('/home')
    }
}

export default userService
